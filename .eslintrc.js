// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
    es6: true,
    jquery: true
  },
  // https://github.com/standard/standard/blob/master/docs/RULES-en.md
  extends: 'standard',
  globals: {
    // Orquestra
    validate: true,
    save: true,
    InsertNewRow: true,
    confirmInputReasonWithValidation: true,
    confirmInputReasonNoValidation: true,
    // Utils
    $u: true,
    Accordion: true,
    Template: true,
    TextTrunk: true,
    Sort: true,
    ActionBtnEvents: true,
    MarkRequired: true,
    TableMv: true,
    MonthPicker: true,
    Placeholder: true,
    addBDay: true
  }
}
