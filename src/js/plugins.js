/* eslint no-unused-vars: 0 */
/* eslint no-eval: 0 */

'use strict'

/**
 * @module
 * Accordion
 *
 * Funciona em conjunto com os estilos e animações da classe `.accordion`,
 * realizando o `toggle` da classe auxiliar `.is-open` através do trigger
 * `[data-accordion-trigger]`
 */
const Accordion = (function () {
  let $triggers

  /**
   * @public
   *
   * Inicializa o plugin buscando pelo trigger
   * através da referência `[data-accordion-trigger]`
   * no DOM
   */
  const init = () => {
    $triggers = $('[data-accordion-trigger]')
    mount($triggers)
  }

  /**
   * @public
   *
   * Adiciona o evento de toggle aos triggers
   * @param {jQuery Object} $triggers
   */
  const mount = $triggers => {
    $triggers.click(toggle)
  }

  /**
   * @private
   *
   * Realiza o toggle da classe `is-open` buscando
   * as referências `accordion-content` e `accordion-item`
   * @param {Object} e - evento de click
   */
  const toggle = function (e) {
    const $this = $(this)
    const $content = $this.next('.accordion-content')
    const $item = $this.closest('.accordion-item')

    $item.toggleClass('is-open')
    $content.stop().slideToggle(250)
  }

  return {
    init,
    mount
  }
})()

//
// ---------------------------------------------------------------------
//

/**
 * @module
 * MarkRequired
 *
 * Atribui a classe auxiliar `.is-required` nos campos obrigatórios,
 * aka `[required="S"]`, de forma dinâmica.
 */
const MarkRequired = (function () {
  /**
   * @public
   * Inicializa o plugin buscando pelas referências `.field-ctrl`
   * e `.table-mv-row.primary` no DOM com o atributo `[required="S"]`,
   * por fim inicializando os métodos de `toggle` e `toggleOnTable`
   */
  const init = () => {
    let $fields = $('.field-ctrl [xname][required="S"]')
    let $tableFields = $('.table-mv-row.primary [xname][required="S"]')

    toggle($fields)
    toggleOnTable($tableFields)
  }

  /**
   * @public
   * Realiza o toggle da classe auxiliar
   * @param {jQuery Object} $fields - campos obrigatórios
   */
  const toggle = $fields => {
    $fields.each(function () {
      $(this)
        .closest('.field')
        .addClass('is-required')
    })
  }

  /**
   * @public
   * Realiza o toggle da classe auxiliar em tabelas, aplicando
   * a mesma na `<td>`
   * @param {jQuery Object} $fields - campos obrigatórios
   */
  const toggleOnTable = $fields => {
    $fields.each(function () {
      let index
      let $label
      let $this = $(this)
      let $table = $this.closest('table')
      let action = $this[0].getAttribute('required') === 'S'
        ? 'add'
        : 'remove'

      if ($table.hasClass('multi-line')) {
        index = $this.closest('[class*="col-"]').index()
        $label = $table.find(`tr.header td:nth-child(2) .grid [class*="col-"]:nth-child(${index + 1})`)
      } else {
        index = $this.closest('td').index()
        $label = $table.find(`tr.header td:nth-child(${index + 1})`)
      }

      helper[action]($label)
    })
  }

  /**
   * @private
   */
  const helper = {
    /**
     * @param {jQuery Object} $label - label do campo
     */
    add ($label) {
      $label.addClass('is-required')
    },

    /**
     * @param {jQuery Object} $label - label do campo
     */
    remove ($label) {
      $label.removeClass('is-required')
    }
  }

  return {
    init,
    toggle,
    toggleOnTable
  }
})()

//
// ---------------------------------------------------------------------
//

/**
 * @module
 * Placeholder
 *
 * Transforma o texto auxiliar nativo do Orquestra ("Ajuda do campo")
 * para placeholder
 */
const Placeholder = (function () {
  /**
   * @public
   * Inicializa o plugin buscando pelas referência `[data-placeholder]`
   * no DOM, limitando apenas a `input`s e `textarea`s
   */
  const init = () => {
    let $inputs = $('[data-placeholder] input[xname], [data-placeholder] textarea[xname]')
    add($inputs)
  }

  /**
   * @public
   * Abstrai a adição do placeholder para campos únicos ou uma coleção
   * @param {jQuery Object | jQuery Object[]} fields - campos, ou coleção
   * de campos para montar o plugin
   */
  const add = fields => {
    $.isArray(fields)
      ? fields.forEach($el => mount($el))
      : mount(fields)
  }

  /**
   * @private
   * Adiciona o placeholder no campo indicado e remove o texto nativo
   * de ajuda
   * @param {jQuery Object} $el - campo do formulário
   */
  const mount = $el => {
    let $em = $el.next('em')

    $el.attr('placeholder', $em.text())
    $em.text('')
  }

  return {
    init,
    add
  }
})()

//
// ---------------------------------------------------------------------
//

/**
 * @module
 * MonthPicker
 *
 * Inicializa o plugin Bootstrap Datepicker utilizado internamente
 * pelo Orquestra com o modo de visualização no formato de `meses`
 *
 * [https://bootstrap-datepicker.readthedocs.io/en/latest/]
 */
const MonthPicker = (function () {
  /**
   * @public
   * @param {Object} params - parâmetros
   * @param {jQuery Object[]} - params.$field - campos do formulário
   */
  const init = params => {
    params.$fields.forEach($field => mount($field))
  }

  /**
   * @private
   * Re-aplica o método construtor do plugin com o modo de visualização
   * `months` e o formato `M/yy`
   * @param {jQuery Object} - $field = campo do formulário
   */
  const mount = $field => {
    $field.datepicker('remove')
    $field.datepicker({
      startView: 'months',
      minViewMode: 'months',
      format: 'M/yy',
      language: 'pt-BR'
    })
  }

  return {
    init
  }
})()

//
// ---------------------------------------------------------------------
//

/**
 * @module
 * Template
 *
 * Gera uma abstração do DOM para um objeto javascript a partir dos campos
 * de formulário do Orquestra
 */
const Template = (function () {
  const values = {}
  let defaults = {
    groupSepartor: '_',
    context: document,
    groupAlways: []
  }

  /**
   * @public
   * Inicializa o módulo configurando as opções passadas
   * por parâmetro
   *
   * @param {Object} options - configurações do módulo
   * @param {String[]} options.ignore - lista de campos a serem
   * ignorados durante o mapeamento
   * @param {String[]} options.groupAlways - prefixo dos campos
   * que devem ser agrupados em um objeto exlusivo no mapeamento
   * @param {String} options.groupSepartor - caracter separador
   * utilizado no nome de campos que estão agrupados, constituídos
   * por `groupName` + `groupSepartor` + `fieldName`
   * @param {Object} options.context - ponto de partida no DOM para
   * realizar o mapeamento dos campos
   */
  const init = (options = {}) => {
    defaults = Object.assign(defaults, options)
    return map(defaults.context)
  }

  /**
   * @private
   * Cria o objeto de abstração do DOM a partir das configurações
   * iniciais
   * @param {Object} context - ponto de partida no DOM
   */
  const map = context => {
    const $els = context.querySelectorAll('input[xname]')

    Array.from($els).forEach(inp => {
      const name = inp.getAttribute('xname').substring(3)

      if (!defaults.ignore.includes(name)) {
        hasGroup(name)
          ? buildGroup(name, inp.value)
          : values[name] = inp.value
      }
    })
    return values
  }

  /**
   * @private
   * Verifica se o nodo do objeto é um groupo a partir
   * da identificação do `options.groupSepartor`
   * @param {String} name - nome do nodo
   * @returns {Boolean}
   */
  const hasGroup = name => name.indexOf(defaults.groupSepartor) >= 0

  /**
   * @private
   * Verifica se um campo faz parte de uma coleção (tabelas multi-valoradas)
   * ou de um grupo (configurado em `options.groupAlways`)
   * @param {String} name - nome do nodo
   * @param {String} group - nome do nodo de grupo
   * @returns {Boolean}
   */
  const hasMany = (name, group) => {
    return (
      defaults.context.querySelectorAll(`[xname=inp${name}`).length > 1 ||
      defaults.groupAlways.includes(group)
    )
  }

  /**
   * @private
   * Constrói o objeto observando se os campos fazem parte de uma coleção
   * grupo ou são singulares
   * @param {String} name - `id` do campo de formulário
   * @param {*} value - valor do campo de formulário
   */
  const buildGroup = (name, value) => {
    const [ group, field ] = name.split(defaults.groupSepartor)

    hasMany(name, group)
      ? add.toMany(group, field, value)
      : add.toSingle(group, field, value)
  }

  /**
   * @private
   */
  const add = {

    /**
     * Adiciona um campo ao objeto raiz
     * @param {String} group - nome do nodo de grupo
     * @param {String} field - nome do campo
     * @param {*} value - valor do campo
     */
    toSingle (group, field, value) {
      if (!values[group]) {
        values[group] = {}
      }
      values[group][field] = value
    },

    /**
     * Adiciona o campo a um grupo e, caso necesário,
     * a uma coleção
     * @param {String} group - nome do nodo de grupo
     * @param {String} field - nome do campo
     * @param {*} value - valor do campo
     */
    toMany (group, field, value) {
      if (!values[group]) {
        values[group] = []
      }
      if (values[group].length === 0) {
        this.toCollection(group, field, value)
      } else {
        const newCollection = values[group]
          .every(group => {
            if (group[field]) {
              return true
            } else {
              group[field] = value
              return false
            }
          })
        if (newCollection) {
          this.toCollection(group, field, value)
        }
      }
    },

    /**
     * Adiciona o campo a uma coleção
     * @param {String} group - nome do nodo de grupo
     * @param {String} field - nome do campo
     * @param {*} value - valor do campo
     */
    toCollection (group, field, value) {
      let pos = values[group].length

      values[group].push({})
      values[group][pos][field] = value
    }
  }

  return {
    init
  }
})()

//
// ---------------------------------------------------------------------
//

/**
 * @module
 * TextTrunk
 *
 * Aplica o recurso `text-trunk` juntamente a um `toggler` em textos
 * longos
 */
const TextTrunk = (function () {
  let $fields
  const defaults = {
    minSize: 200,
    showLabel: 'Ver mais',
    hideLabel: 'Ver menos'
  }

  /**
   * @public
   * Monta o plugin com o texto passado via parâmetro
   * @param {String} text - texto para aplicação do plugin
   * @param {Number} minSize - tamanho minímo para considerar a
   * aplicação do `text-trunk`
   */
  const mount = (text, minSize) => {
    let size = minSize || defaults.minSize
    let len = text ? text.length : 0

    return len > size
      ? render(text)
      : text
  }

  /**
   * @private
   * @param {String} text - texto para aplicação do plugin
   * @return DOM Nodes - nova estrutura com o plugin aplicado
   */
  const render = text => {
    return `
      <div class="text-trunk">
        <p class="text-trunk-less">
          <span>${text.substring(0, defaults.minSize)}... </span>
          <span class="text-trunk-trigger" onclick="TextTrunk.toggle(this)">
            ${defaults.showLabel}
          </span>
        </p>
        <p class="text-trunk-all hidden">
          <span>${text}</span>
          <span class="text-trunk-trigger" onclick="TextTrunk.toggle(this)">
            ${defaults.hideLabel}
          </span>
        </p>
      </div>`
  }

  /**
   * @public
   * Alterna entre os estados `all` e `less` através de classes auxiliares,
   * exibindo e ocultando o texto.
   * @param {DOM Element} el - trigger 'Ver Mais... | Ver menos ...'
   */
  const toggle = el => {
    const $el = $(el)
    const $wrapper = $el.closest('.text-trunk')
    const $text = $wrapper.find('.text-trunk-all, .text-trunk-less')

    $text.toggleClass('hidden')
  }

  return {
    mount,
    toggle
  }
})()

//
// ---------------------------------------------------------------------
//

/**
 * @module
 * Notifier
 *
 * Dispara alertas de notificação na tela
 */
const Notifier = (function () {
  /**
   * Elemento `wrapper` para viabilizar a criação de elementos
   * `.notifier` na tela
   */
  const $wrapper = $('<div></div>')
    .addClass('notifier-wrapper')

  /**
   * @public
   * Inicializa o plugin adicionando o `wrapper` ao DOM
   */
  const init = () => {
    $('body').prepend($wrapper)
  }

  /**
   * @private
   * Remove um elemento de notificação
   * @param {jQuery Object} $el - elemento notificação
   */
  const clear = $el => {
    $el.removeClass('is-active')
    setTimeout(() => $el.remove(), 300)
  }

  /**
   * @public
   * Cria e exibe uma nova notificação a partir do texto passado por
   * parâmetro e remove automaticamente da tela após um intervalo de tempo
   * @param {String} msg - texto da notificação
   * @param {String} modifier - classe auxiliar para estado da notificação
   */
  const show = (msg, modifier) => {
    const $wrapper = $('.notifier-wrapper')
    const $el = $('<div></div>').addClass('alert notifier')
    const $msg = $(`<p>${msg}</p>`).addClass('notifier-message')

    $el
      .addClass(`alert-${modifier}`)
      .append($msg)

    $wrapper.append($el)
    setTimeout(() => $el.addClass('is-active'), 100)
    setTimeout(() => clear($el), 6000)
  }

  return {
    init,
    show
  }
})()

//
// ---------------------------------------------------------------------
//

/**
 * @module
 * Autosize
 *
 * Aplica o plugin `autosize` a campos do tipo `textarea`, aumentando o
 * tamanho do campo conforme são adicionadas novas linhas
 */
const Autosize = (function () {
  /**
   * Tamanho base do campo (somente uma linha visível)
   */
  const BASE_HEIGHT = 30

  /**
   * @public
   * Adiciona os eventos aos campos para funcionamento do plugin
   * @param {jQuery Object} $textarea - campo textarea
   */
  const mount = $textarea => {
    if ($textarea.length > 0) {
      $textarea
        .addClass('autosize')
        .on('input focus', setHeight)

      setHeight.call($textarea[0])
    }
  }

  /**
   * @private
   * Calcula a altura do campo conforme o número de linhas do texto,
   * baseado na informação `scrollHeight`
   */
  const setHeight = function () {
    this.style.height = 0

    let lines = (this.scrollHeight + 2) / BASE_HEIGHT
    let size = lines === 1 ? BASE_HEIGHT : lines * BASE_HEIGHT

    this.style.height = `${size < 30 ? 30 : size}px`
  }

  return {
    mount
  }
})()

//
// ---------------------------------------------------------------------
//

/**
 * @module
 * ToList
 *
 * Transforma a renderização nativa do orquestra para campos do tipo `checkbox`
 * quando somente leitura, dispostos no formato `inline` para uma lista `ul`
 */
const ToList = (function () {
  const $list = $('<ul></ul>')

  /**
   * @public
   * Inicializa o plugin buscando pelas referência `[data-to-list]` no
   * DOM convertando o valor do campo em uma lista
   */
  const init = () => {
    const $fields = $('[data-to-list]')

    if ($fields.length > 0) {
      mount($fields)
    }
  }

  /**
   * @private
   * Adiciona a lista `ul` ao DOM, busca os campos e esconde os labels
   * nativos, `div[xid]`
   * @param {jQuery Object} $fields - campos para transformação
   */
  const mount = $fields => {
    $fields
      .find('input[type="hidden"][xname]')
      .each(render)

    $fields
      .append($list)
      .find('div[xid]')
      .hide()
  }

  /**
   * @private
   * Adiciona um `li` com o valor do campo
   */
  const render = function () {
    const val = $(this).val()

    if (val !== '') {
      $list.append(`<li>${val}</li>`)
    }
  }

  return {
    init
  }
})()

//
// ---------------------------------------------------------------------
//

/**
 * @module
 * Sort
 *
 * Reordena campos do Orquestra considerando seu <nome|formato> permitindo
 * que a ordenação não seja estritamente de forma alfabética.
 *
 * Para isso é preciso utilizar o formato `<número>. <valor da fonte de dados>`
 * na fonte de dados.
 *
 * O plugin irá remover de forma `silenciosa`¹ o prefixo `<número>. ` que
 * está determinando a ordenação, deixando somente o `<valor da fonte de dados>`
 * da fonte de dados.
 *
 * ¹: Para evitar reescrever o valor real da fonte de dados somente é alterado
 * o label/valor do campo em tempo de execução. Isto evita que em casos onde o
 * campo já está com um valor salvo essa informação não se perca, dado que o valor
 * da fonte de dados no formato `<valor da fonte de dados>` (sem a numeração) não
 * existe na realidade, e, caso fosse salvo este valor, durante a renderização onde
 * o campo já está com um valor salvo este seria renderizado sem nenhum valor
 * definido.
 */
const Sort = (function () {
  let type

  /**
   * @public
   * Inicializa o plugin buscando pelas referências `[data-sort]` no DOM,
   * restringindo somente a campos visíveis
   */
  const init = () => {
    let $fields = [
      $('[data-sort]:visible > select'),
      $('[data-sort]:visible > input[type=hidden]')
    ].filter($field => $field.length > 0)

    render($fields)
  }

  /**
   * @public
   * Abstrai a adição da ordenação para campos únicos ou uma coleção
   * @param {jQuery Object | jQuery Object[]} fields - campos, ou coleção
   * de campos do formulário
   */
  const render = fields => {
    $.isArray(fields)
      ? fields.forEach($el => mount($el))
      : mount(fields)
  }

  /**
   * @private
   * Renomeia o valor do campo removento o prefixo:
   * `<número>. `
   * @param {jQuery Object} $el - campo de formulário
   */
  const mount = $el => {
    type = $el[0].type
    $el.each(rename.handle)
  }

  /**
   * @private
   */
  const rename = {
    /**
     * Verifica o tipo de campo
     */
    handle () {
      let $this = $(this)
      let value = $this.val()

      if (type === 'select-one') {
        return $this.find('option').each(rename.select)
      }

      if (rename.evaluate(value)) {
        return rename[type]($this, value)
      }
    },

    /**
     * Renomeia campos do tipo `input[typ=radio]`
     * @param {jQuery Object} $el - campo de formulário
     * @param {String} value - valor da fonte de dados
     */
    radio ($el, value) {
      let newValue = value.substring(3)

      $el
        .closest('label')
        .contents()[1].nodeValue = newValue
    },

    /**
     * Renomeia campos do tipo `select`
     */
    select () {
      let $this = $(this)
      let value = $this.val()

      if (value && rename.evaluate(value)) {
        $this.text(value.substring(3))
      }
    },

    /**
     * Renomeia campos do tipo somente leitura, `input[typ=hidden]`
     */
    hidden ($el, value) {
      let id = $el.attr('xname').substring(3)
      $(`[xid="div${id}"]`).text(value.substring(3))
    },

    /**
     * TODO: Renomeia campos do tipo `input[typ=checkbox]`
     */
    checkbox () {
      console.error('Sort Fn => Campo `checkbox` não implementado')
    },

    /**
     * Valida se o formato da fonte de dados atende a formatação
     * do plugin: `<número>. <valor da fonte de dados>`
     * @param {String} value - valor da fonte de dados
     */
    evaluate (value) {
      return value === ''
        ? false
        : isNaN(parseInt(value.substring(0, 1)))
          ? console.warn('Sort Fn => A a fonte de dados deve estar precedida por números - e.g. "1. "')
          : true
    }
  }

  return {
    init,
    render
  }
})()

//
// ---------------------------------------------------------------------
//

/**
 * @module
 * TableMv
 *
 * Estende as tabelas multi-valoradas do Orquestra permitindo o uso de callbacks
 * durante a renderização da tabela e ao adicionar novas linhas
 */
const TableMv = (function () {
  let $btnInsert
  let $tbl
  let callback

  /**
   * @public
   * Inicializa o plugin chamando o callback inicial `params.mount`
   * @param {Object} params - parâmetros de configuração
   * @param {jQuery Object} $table - tabela multi-valorada
   * @param {Function} events - callback chamado ao adicionar novas linhas
   * @param {Object} mount - callback chamado ao renderizar a tabela
   */
  const init = params => {
    mount(params)

    if (params.mount) {
      params.mount()
    }
  }

  /**
   * @public
   * Remonta a tabela multi-valorada, chamando o callback `params.events`
   * para todas as linhas presentes na tabela na primeira renderização
   * @param {Object} params - parâmetros de configuração
   * @param {jQuery Object} $table - tabela multi-valorada
   * @param {Function} events - callback chamado ao adicionar novas linhas
   * @param {Object} mount - callback chamado ao renderizar a tabela
   */
  const mount = params => {
    setParams(params)
    setTriggers()

    if (callback) {
      getRow.all().each(function () {
        callback($(this))
      })
    }
  }

  /**
   * @private
   * Define os parâmetros globais do módulo
   * @param {jQuery Object} $table - tabela multi-valorada
   * @param {Function} events - callback chamado ao adicionar novas linhas
   */
  const setParams = ({ $table, events }) => {
    $tbl = $table
    callback = events
    $btnInsert = $table.find('#BtnInsertNewRow')
  }

  /**
   * @private
   * Remonta o evento de `click` do botão Inserir para permitir
   * a chamada do callback `params.events`
   */
  const setTriggers = () => {
    $btnInsert
      .removeAttr('onclick')
      .off('click')
      .on('click', addRow.bind(this))
  }

  /**
   * @private
   * Insere uma nova linha na tabela e chama o callback `params.events`
   * passando como parâmetro a nova linha adicionada
   * @param {DOM Element} currentTarget - botão Inserir, utilizado pela função
   * nativa do Orquestra `InsertNewRow`
   */
  const addRow = ({ currentTarget }) => {
    InsertNewRow(currentTarget, true)

    if (callback) {
      callback(getRow.last())
    }
  }

  /**
   * @private
   */
  const getRow = {
    /**
     * @returns {jQuery Object} - última linha da tabela
     */
    last () {
      return $tbl.find('tr:last')
    },

    /**
     * @returns {jQuery Object} - todas as linhas da tabela,
     * exceto o `.header`
     */
    all () {
      return $tbl.find('tr:not(.header)')
    }
  }

  return {
    init,
    mount
  }
})()

//
// ---------------------------------------------------------------------
//

/**
 * @module
 * ActionBtnEvents
 *
 * Estende os botões de finalização do Orquestra adicionando novos eventos,
 * permitindo maior flexibilidade no disparo de ações vínculados a finalização
 * das atividades.
 *
 * Eventos:
 *
 * - `actionClick`: chamado logo após click no botão
 *
 * - `validationPass`: chamado somente quando o formulário passar na
 * função de validação nativa do Orquestra `validate`
 *
 * - `validationFail`: chamado somente quando o formulário não passar
 * na função de validação nativa do Orquestra `validate`
 */
const ActionBtnEvents = (function () {
  const $btnConfirm = $('#BtnConfirmReason')
  const defActions = [
    'conclude()',
    'approve()',
    'send()'
  ]
  const action = {}
  let tracker

  /**
   * @public
   */
  const init = () => {
    registerEvents()
    tracker = setInterval(track, 500)
  }

  /**
   * @private
   * Registrar os novos eventos utilizados nos botões de finalização
   */
  const registerEvents = () => {
    action.click = new Event('actionClick')
    action.pass = new Event('validationPass')
    action.fail = new Event('validationFail')
  }

  /**
   * @private
   * Garante a aplicação a função `register` em todos os botões da área
   * de finalização (presentes no bloco `#buttons`)
   *
   * Os botões de finalização são renderizados através de um chamada
   * http (ajax) externa, diferente do formulário
   */
  const track = () => {
    let btns = [...document.querySelectorAll('#buttons > button')]
    if (btns.length > 0) {
      btns.forEach(btn => register(btn))
      clearInterval(tracker)
    }
  }

  /**
   * @private
   * Registra um handler para ser executado no evento de `click`
   * do botão e guarda a referência antiga na propriedade `data-action`
   * @param {DOM Element} btn - botão de finalização
   */
  const register = btn => {
    let action = btn.getAttribute('onclick')
    btn.dataset.action = action
    btn.removeAttribute('onclick')
    btn.addEventListener('click', handleClick)
  }

  /**
   * @private
   * Trata o evento de `click` do botão, chamando os novos eventos,
   * abstraindo a chamada destes quando for um botão com preenchimento
   * de justificativa ou sem validação:
   *
   * @param {Object} e - evento de click
   */
  const handleClick = e => {
    const fn = e.target.dataset.action
    const [
      name,
      hasValidation,
      hasJustification
    ] = fn.substring(8).replace(/\(|\)/g, '').split(',')

    if (hasJustification === 'true') {
      eval(fn) // para exibir o campo de justificativa

      $('#BtnConfirmReason').off('click').click(() => {
        e.target.dispatchEvent(action.click)

        hasValidation === 'true'
          ? handleValidation(e.target, confirmInputReasonWithValidation)
          : confirmInputReasonNoValidation()
      })
    } else {
      e.target.dispatchEvent(action.click)

      if (
        hasValidation === 'true' ||
        defActions.includes(fn)
      ) {
        handleValidation(e.target, validate)
      }
      eval(fn)
    }
  }

  /**
   * @private
   * @param {DOM Element} el - botão de finalização
   * @param {Function} fnValidate - função de validação do formulário
   */
  const handleValidation = (el, fnValidate) => {
    fnValidate()
      ? el.dispatchEvent(action.pass)
      : el.dispatchEvent(action.fail)
  }

  return {
    init
  }
})()
