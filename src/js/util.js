const $util = (function () {
  return {
    alias () {
      return document.getElementById('inpDsFlowElementAlias').value
    },

    inp (id) {
      return $(`[xname="inp${id}"]`)
    },

    type (id) {
      return this.inp(id)[0].type
    },

    values (id) {
      let $inp = this.inp(id)
      this.valuesOf($inp)
    },

    valuesOf ($inp) {
      let type = $inp[0].type

      if (['text', 'textarea', 'select-one', 'hidden'].includes(type)) {
        return [$inp.val()]
      } else if (type === 'radio') {
        return [$inp.filter(':checked').val()]
      } else if (type === 'checkbox') {
        return $inp
          .filter(':checked')
          .toArray()
          .map(el => el.value)
      } else {
        console.warn('Util.values fn : undefined type')
      }
    },

    handler ({ id, event }, callback) {
      if (!callback) {
        return console.warn('Util.handler fn : parâmetros mal formatados')
      }
      const $inp = this.inp(id)

      const compare = values => {
        for (let i = 0; i < callback.length; i += 2) {
          if ($.isFunction(callback[i])) {
            return callback[i](values)
          }
          if (hasValue(values, callback[i])) {
            return callback[i + 1](values)
          }
        }
      }

      const hasValue = (values, val) => {
        if (typeof val === 'string') {
          return values.includes(val)
        }
        if ($.isArray(val)) {
          return values
            .some(any => val.includes(any))
        }
        return false
      }

      const evaluate = ({ target }) => {
        const values = this.valuesOf($(target))

        $.isFunction(callback)
          ? callback(values, target)
          : $.isArray(callback)
            ? compare(values)
            : console.warn('Util.handler fn : callback precisa ser uma function ou um array')
      }

      $inp.on(event, evaluate)
      evaluate({ target: $inp[0] })
    },

    table: {

      show ({ $table, wrapper = '.section' }) {
        $table
          .closest(wrapper)
          .removeClass('hidden')

        this.require($table)
      },

      hide ({ $table, wrapper = '.section' }) {
        $table
          .closest(wrapper)
          .addClass('hidden')

        this.clear($table)
        this.removeRequire($table)
      },

      clear ($table) {
        $table.find('#BtnInsertNewRow').click()
        $table.find('tr:not(:last):not(.header)').remove()
      },

      require ($table) {
        const fields = $table[0].querySelectorAll('[data-required]')
        Array
          .from(fields)
          .forEach(inp => {
            inp.setAttribute('required', 'S')
            inp.removeAttribute('data-required')
          })
      },

      removeRequire ($table) {
        const fields = $table[0].querySelectorAll('[required="S"]')
        Array
          .from(fields)
          .forEach(inp => {
            inp.setAttribute('required', 'N')
            inp.dataset.required = true
          })
      }

    }
  }
})()

const $u = $util
